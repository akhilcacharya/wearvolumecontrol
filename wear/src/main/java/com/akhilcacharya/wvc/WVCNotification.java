package com.akhilcacharya.wvc;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;


public class WVCNotification {

    private static final String NOTIFICATION_TAG = "WVC";

    public static final String ACTION_SILENCE = "SILENCE_ACTION";
    public static final String ACTION_VIBRATE = "SILENCE_VIBRATE";

    public static void notify(final Context context) {
        final Resources res = context.getResources();

        // This image is used as the notification's large icon (thumbnail).
        // TODO: Remove this if your notification has no relevant thumbnail.
        final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.example_picture);


        final String ticker = "Wear Volume Control";
        final String title = ticker;
        final String text = ticker;

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)

                // Set appropriate defaults for the notification light, sound,
                // and vibration.
                .setDefaults(Notification.DEFAULT_ALL)

                // Set required fields, including the small icon, the
                // notification title, and text.
                .setSmallIcon(R.drawable.ic_stat_wvc)
                .setContentTitle(title)
                .setContentText(text)

                // All fields below this line are optional.

                // Use a default priority (recognized on devices running Android
                // 4.1 or later)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                // Provide a large icon, shown with the notification in the
                // notification drawer on devices running Android 3.0 or later.
                .setLargeIcon(picture)

                // Set ticker text (preview) information for this notification.
                .setTicker(ticker)

                .setOngoing(true)


                // If this notification relates to a past or upcoming event, you
                // should set the relevant time information using the setWhen
                // method below. If this call is omitted, the notification's
                // timestamp will by set to the time at which it was shown.
                // TODO: Call setWhen if this notification relates to a past or
                // upcoming event. The sole argument to this method should be
                // the notification timestamp in milliseconds.
                //.setWhen(...)

                // Show expanded text content on devices running Android 4.1 or
                // later.
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(text)
                        .setBigContentTitle(title)
                        .setSummaryText("Dummy summary text"));


        Intent actionIntent = new Intent(context, WearVolumeControl.class);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(context, 0, actionIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        Intent silenceIntent = new Intent(context, HandleCommandService.class);
        silenceIntent.setAction(ACTION_SILENCE);
        PendingIntent silencePendingIntent =
                PendingIntent.getActivity(context, 1, silenceIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        Intent vibrateIntent = new Intent(context, HandleCommandService.class);
        silenceIntent.setAction(ACTION_VIBRATE);
        PendingIntent vibratePendingIntent =
                PendingIntent.getActivity(context, 2, vibrateIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Action action = new NotificationCompat.Action.Builder(R.drawable.ic_stat_wvc, "Control Volume", actionPendingIntent).build();
        NotificationCompat.Action silence = new NotificationCompat.Action.Builder(R.drawable.ic_stat_wvc, "Silence", silencePendingIntent).build();
        NotificationCompat.Action vibrate = new NotificationCompat.Action.Builder(R.drawable.ic_stat_wvc, "Vibrate", vibratePendingIntent).build();


        NotificationCompat.WearableExtender extender = new NotificationCompat.WearableExtender();
        extender.setHintHideIcon(true);
        extender.addAction(action);
        extender.addAction(silence);
        extender.addAction(vibrate);

        builder.extend(extender);

        notify(context, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManagerCompat nm = NotificationManagerCompat.from(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.notify(NOTIFICATION_TAG.hashCode(), notification);
        }
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.cancel(NOTIFICATION_TAG, 0);
        } else {
            nm.cancel(NOTIFICATION_TAG.hashCode());
        }
    }

}