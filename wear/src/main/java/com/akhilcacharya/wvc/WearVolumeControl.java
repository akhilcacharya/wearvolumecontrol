package com.akhilcacharya.wvc;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

public class WearVolumeControl extends Activity{

    private SeekBar seekVolume;
    private static TextView volumeIndex;

    private GoogleApiClient client;
    private AudioManager manager;

    private static String TAG = "WearVolumeControl";
    private static final String VOLUME_CHANGE_PATH = "/wvc/volume";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wear_volume_control);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                //setup Google API Client
                client = new GoogleApiClient.Builder(getBaseContext())
                        .addApi(Wearable.API)
                        .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(ConnectionResult connectionResult) {
                                Log.v(TAG, "Connection failed :(");
                            }
                        })
                        .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                            @Override
                            public void onConnected(Bundle bundle) {
                                Log.v(TAG, "Connection!");
                            }

                            @Override
                            public void onConnectionSuspended(int i) {
                                Log.v(TAG, "Disconnected");
                            }
                        }).build();

                client.connect();

                manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

                seekVolume = (SeekBar) findViewById(R.id.activity_wear_volume_seek);
                volumeIndex = (TextView) findViewById(R.id.activity_wear_volume_control_index);

                seekVolume.setMax(manager.getStreamMaxVolume(AudioManager.STREAM_RING));

                seekVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        PutDataMapRequest dataMap = PutDataMapRequest.create(VOLUME_CHANGE_PATH);
                        dataMap.getDataMap().putLong("time", System.currentTimeMillis());
                        dataMap.getDataMap().putInt(VOLUME_CHANGE_PATH, progress);

                        PutDataRequest request = dataMap.asPutDataRequest();
                        Wearable.DataApi.putDataItem(client, request);
                        volumeIndex.setText("Volume: " + progress + "/" + seekVolume.getMax());
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });


            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        client.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.disconnect();
    }
}
