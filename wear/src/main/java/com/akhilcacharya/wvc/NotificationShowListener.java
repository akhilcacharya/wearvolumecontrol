package com.akhilcacharya.wvc;


import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

public class NotificationShowListener extends WearableListenerService {

    private static final String TAG = "NotificationShowListener";
    private GoogleApiClient client;

    @Override
    public void onCreate() {
        super.onCreate();

        //Check if connected to any nodes
        //If yes, show notification

        client = new GoogleApiClient.Builder(getBaseContext())
                .addApi(Wearable.API)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.v(TAG, "Connection failed :(");
                    }
                })
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.v(TAG, "Connection!  :(");

                        Wearable.NodeApi.getConnectedNodes(client).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                            @Override
                            public void onResult(NodeApi.GetConnectedNodesResult getConnectedNodesResult) {
                                Log.v(TAG, "More than 0 connected nodes!" + getConnectedNodesResult.getNodes().size());
                                if(getConnectedNodesResult.getNodes().size() > 0){
                                    WVCNotification.notify(getBaseContext());
                                    Log.v(TAG, "More than 0 connected nodes!");
                                }
                            }
                        });
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.v(TAG, "Disconnected");
                    }
                }).build();

        client.connect();

    }

    @Override
    public void onPeerConnected(Node peer) {
        super.onPeerConnected(peer);
        //Show notification
        WVCNotification.notify(getBaseContext());
    }

    @Override
    public void onPeerDisconnected(Node peer) {
        super.onPeerDisconnected(peer);
        //Remove notification
        WVCNotification.cancel(getBaseContext());
    }





}
