package com.akhilcacharya.wvc;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class HandleCommandService extends IntentService {

    private static final String TAG = "HandleCommandService";

    private static final String VIBRATE_PATH = "/wvc/vibrate";
    private static final String SILENCE_PATH = "/wvc/silence";

    public HandleCommandService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final String action = intent.getAction();

        final GoogleApiClient client = new GoogleApiClient.Builder(getBaseContext())
                .addApi(Wearable.API)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.v(TAG, "Connection failed :(");
                    }
                })
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.v(TAG, "Connection!");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.v(TAG, "Disconnected");
                    }
                }).build();

        client.connect();

        Wearable.NodeApi.getConnectedNodes(client).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult getConnectedNodesResult) {
                Node node = getConnectedNodesResult.getNodes().get(0);
                if(node != null){
                    MessageApi.SendMessageResult result = null;
                    if(WVCNotification.ACTION_SILENCE.equals(action)){
                        //Send silence message
                        result = Wearable.MessageApi.sendMessage(client, node.getId(), VIBRATE_PATH, null).await();
                        Log.v(TAG, "Accepted silence action, sending message");
                    }else if(WVCNotification.ACTION_VIBRATE.equals(action)) {
                        //Send vibrate message
                        Log.v(TAG, "Accepted vibrate action, sending message");
                        result = Wearable.MessageApi.sendMessage(client, node.getId(), VIBRATE_PATH, null).await();
                    }

                   if(result.getStatus().isSuccess()) {
                        Intent confirmationIntent = new Intent(getBaseContext(), ConfirmationActivity.class);
                        confirmationIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
                        confirmationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(confirmationIntent);
                   }else {
                       Intent confirmationIntent = new Intent(getBaseContext(), ConfirmationActivity.class);
                       confirmationIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.FAILURE_ANIMATION);
                       confirmationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       startActivity(confirmationIntent);
                   }
                }
            }
        });
        }

    }

