package com.akhilcacharya.wvc;


import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;

public class VolumeListenerService extends WearableListenerService{

    private AudioManager manager;
    private GoogleApiClient client;

    private static final String TAG = "VolumeListenerService";
    private static final String VOLUME_CHANGE_PATH = "/wvc/volume";
    private static final String VIBRATE_PATH = "/wvc/vibrate";
    private static final String SILENCE_PATH = "/wvc/silence";


    @Override
    public void onCreate() {
        super.onCreate();
        manager = (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);

        //setup Google API Client
        client = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.v(TAG, "Connection failed :(");
                    }
                })
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.v(TAG, "Connection!");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.v(TAG, "Disconnected");
                    }
                })
                .build();


        client.connect();

    }

    //Receive Message for Vibration and silence
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        if(messageEvent.getPath().contains(VIBRATE_PATH)){
            setVibration(true);
            setVolume(0);
            Toast.makeText(getBaseContext(), "Enabled vibration", Toast.LENGTH_LONG).show();
        }else if(messageEvent.getPath().contains(SILENCE_PATH)){
            setVibration(false);
            setVolume(0);
            Toast.makeText(getBaseContext(), "Enabled silence", Toast.LENGTH_LONG).show();
        }
    }

    //Get volume
    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);
        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);

        for(DataEvent event: events){
            Log.v(TAG, event.getDataItem().getUri().toString());
            if(event.getDataItem().getUri().toString().contains(VOLUME_CHANGE_PATH)){
                DataMapItem item = DataMapItem.fromDataItem(event.getDataItem());
                int volume = item.getDataMap().getInt(VOLUME_CHANGE_PATH);
                setVolume(volume);
                Log.v(TAG, "Volume: " + volume);
            }
        }
    }


    private void setVolume(int convertedVolume){
        Log.v(TAG, "Converted volume " + convertedVolume + " max ring " + manager.getStreamMaxVolume(AudioManager.STREAM_RING));
        manager.setStreamVolume(AudioManager.STREAM_RING, convertedVolume, AudioManager.FLAG_SHOW_UI);
    }

    private void setVibration(boolean isOn){
        if(isOn){
            manager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        }else{
            manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        }
    }



}
